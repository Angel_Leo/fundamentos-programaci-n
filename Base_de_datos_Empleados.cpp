//Regisro de Empleados 
/*Proyecto Final
Equipo: Angel Leonardo Barrios Estrada
        Edgar Jair Flores Vel�squez
        */
#include <iostream>
#include <conio.h>
#include <string.h>
#include <fstream>
#include <stdlib.h>
//Limpiar pantalla
#ifdef _WIN32
#define CLEAR "cls"
#elif defined(unix)||defined(__unix__)||defined(__unix)||defined(__APPLE__)||defined(__MACH__)
#define CLEAR "clear"
#else
#error "SO no soportado para limpiar pantalla"
#endif

using namespace std;

class menu
{
private:
    string nombre;
    string domicilio;
    string celular;
    string FechaPago;
    string codigoEmpleado;

public:
    void altaEmpleado();
    void bajaEmpleado();
    void modificarEmpleado();
    void listarEmpleado();
    void menuPrincipal();
    void detallesEmpleado();
    void mostarRegistro(string codigo);
};

void pausa();
void error();

void menu::menuPrincipal()
{
    int opcion;
    do
    {
        cout<<"***Registro de Empleados***";
        cout<<"\n\n1. Dar de alta un Empleado\n\n";
        cout<<"2. Mostrar detalles de un Empleado\n\n";
        cout<<"3. Listar Empleados\n\n";
        cout<<"4. Dar de baja un Empleado\n\n";
        cout<<"5. Modificar un Empleado\n\n";
        cout<<"6. Salir\n\n";
        cout<<"Opci\242n: ";
        cin>>opcion;
        system(CLEAR);
        switch(opcion)
        {
        default:
            cout<<"Ha ingresado una opci\242n no valida!\n\n";
            break;

        case 1:
            altaEmpleado();
            break;

        case 2:
            detallesEmpleado();
            break;

        case 3:
            listarEmpleado();
            break;

        case 4:
            bajaEmpleado();
            break;

        case 5:
            modificarEmpleado();
            break;

        case 6:
            break;
        }
    }
    while(opcion!=6);
}

void menu::altaEmpleado()
{
    ofstream escritura;
    ifstream verificador;
    string auxCodigo;
    bool coincidencia=false;
    verificador.open("Empleados.txt",ios::in);
    escritura.open("Empleados.txt",ios::app);
    if(escritura.is_open()&&verificador.is_open())
    {
        cout<<"***Dar de alta un Empleado***";
        fflush(stdin);
        cout<<"Ingresa el c\242digo del Empleado: ";
        getline(cin,auxCodigo);
        if(auxCodigo=="")
            do
            {
                cout<<"c\242digo de Empleado no v\240lido!, intentalo nuevamente: ";
                getline(cin,auxCodigo);
            }
            while(auxCodigo=="");
        do
        {
            verificador.seekg(0);
            getline(verificador,codigoEmpleado);
            while(!verificador.eof())
            {
                getline(verificador,nombre);
                getline(verificador,domicilio);
                getline(verificador,celular);
                getline(verificador,FechaPago);

                if(codigoEmpleado==auxCodigo)
                {
                    coincidencia=true;
                    cout<<"\n\nYa existe un Empelado con ese c\242digo!\n\n";
                    cout<<"\n\nEl Empleado con ese c\242digo es: "<<nombre<<"\n\n";
                    cout<<"Ingresa un c\242digo v\240lido!: ";
                    getline(cin,auxCodigo);
                    if(auxCodigo=="")
                        do
                        {
                            cout<<"\nc\242digo de Empleado no v\240lido!, intentalo nuevamente: ";
                            getline(cin,auxCodigo);
                        }
                        while(auxCodigo=="");

                    break;
                }

                getline(verificador,codigoEmpleado);
            }

            if(verificador.eof()&&auxCodigo!=codigoEmpleado)
                coincidencia=false;

        }
        while(coincidencia==true);
        system(CLEAR);
        codigoEmpleado=auxCodigo;
        cout<<"***Dar de alta un Eempleado***";
        cout<<"\n\nIngresa el c\242digo del Empleado: ";
        cout<<codigoEmpleado;
        cout<<"\n\n";
        fflush(stdin);
        cout<<"\n\nIngresa el nombre del Empleado: ";
        getline(cin,nombre);
        cout<<"\n\n";
        fflush(stdin);
        cout<<"\n\nIngresa el domicilio del Empleado: ";
        getline(cin,domicilio);
        cout<<"\n\n";
        fflush(stdin);
        cout<<"\n\nIngresa el n\243mero de telefono del Empleado: ";
        getline(cin,celular);
        cout<<"\n\n";
        fflush(stdin);
        cout<<"\n\nIngresa la fecha de inscripci\242n del Empleado: ";
        getline(cin,FechaPago);
        cout<<"\n\n";

        escritura<<codigoEmpleado<<"\n"<<nombre<<"\n"<<domicilio<<"\n"<<celular
                 <<"\n"<<FechaPago<<"\n";

        cout<<"\n\nEl registro se ha completado correctamente.\n\n";
    }

    else
    {
        error();
    }

    escritura.close();
    verificador.close();
    pausa();
}

void menu::bajaEmpleado()
{
    ifstream lectura;
    ofstream auxiliar;
    bool encontrado=false;
    string auxCodigo;
    char respuesta[5];
    lectura.open("Empleados.txt",ios::in);
    auxiliar.open("auxiliar.txt",ios::app);
    cout<<"***Dar de baja un Empleado***";
    if(lectura.is_open()&&auxiliar.is_open())
    {
        fflush(stdin);
        cout<<"\n\nIngresa el c\242digo del Empleado que deseas dar de baja: ";
        getline(cin,auxCodigo);
        getline(lectura,codigoEmpleado);
        while(!lectura.eof())
        {
            getline(lectura,nombre);
            getline(lectura,domicilio);
            getline(lectura,celular);
            getline(lectura,FechaPago);

            if(auxCodigo==codigoEmpleado)
            {
                encontrado=true;
                cout<<"\n\nRegistro Encontrado\n\n";
                cout<<"\n\nC\242digo: "<<codigoEmpleado<<endl;
                cout<<"Nombre: "<<nombre<<endl;
                cout<<"Domicilio: "<<domicilio<<endl;
                cout<<"Celular: "<<celular<<endl;
                cout<<"Fecha de inscripci\242n: "<<FechaPago<<endl;
                cout<<"\n\n";
                cout<<"Realmente deseas dar de baja a: "<<nombre<<" (s/n)?: ";
                cin.getline(respuesta,5);
                if(strcmp(respuesta,"s")==0||strcmp(respuesta,"S")==0||
                        strcmp(respuesta,"si")==0||strcmp(respuesta,"SI")==0||
                        strcmp(respuesta,"Si")==0||strcmp(respuesta,"sI")==0)
                {
                    cout<<"El Empleado se ha dado de baja correctamente";
                }

                else
                {

                    cout<<"Empleado conservado";
                    auxiliar<<codigoEmpleado<<"\n"<<nombre<<"\n"<<domicilio<<"\n"<<celular
                            <<"\n"<<FechaPago<<"\n";

                }

            }
            else
            {
                auxiliar<<codigoEmpleado<<"\n"<<nombre<<"\n"<<domicilio<<"\n"<<celular
                        <<"\n"<<FechaPago<<"\n";
            }
            getline(lectura,codigoEmpleado);
        }
        if(encontrado==false)
        {
            cout<<"\n\nNo se encontr\242 el c\242digo: "<<auxCodigo<<"\n\n";
        }

    }
    else
    {
        error();
    }
    lectura.close();
    auxiliar.close();
    remove("Empleados.txt");
    rename("auxiliar.txt","Empleados.txt");
    pausa();
}

void menu::detallesEmpleado()
{
    string auxCodigo;
    ifstream mostrar;
    bool encontrado=false;
    mostrar.open("Empleados.txt",ios::in);
    if(mostrar.is_open())
    {
        fflush(stdin);
        cout<<"***Consultar detalles de un Empleado***";
        cout<<"\n\nIngresa el c\242digo del cliente que deseas consultar detalles: ";
        getline(cin,auxCodigo);
        getline(mostrar,codigoEmpleado);
        while(!mostrar.eof())
        {
            getline(mostrar,nombre);
            getline(mostrar,domicilio);
            getline(mostrar,celular);
            getline(mostrar,FechaPago);

            if(auxCodigo==codigoEmpleado)
            {
                encontrado=true;
                cout<<"\n\nRegistro Encontrado\n\n";
                cout<<"C\242digo: "<<codigoEmpleado<<endl;
                cout<<"Nombre: "<<nombre<<endl;
                cout<<"Domicilio: "<<domicilio<<endl;
                cout<<"Celular: "<<celular<<endl;
                cout<<"Fecha de inscripci\242n: "<<FechaPago<<endl;
                cout<<"\n\n";
            }

            getline(mostrar,codigoEmpleado);
        }

        if(encontrado==false)
        {
            cout<<"\n\nNo se encontro el registro: "<<auxCodigo<<"\n\n";
        }
    }

    else
    {
        error();
    }

    mostrar.close();
    pausa();
}

void menu::modificarEmpleado()
{
    ifstream lectura;
    ifstream verificador;
    ofstream auxiliar;
    string auxCodigo;
    string codigoModif;
    string auxNombre;
    string auxDomicilio;
    string auxCelular;
    string auxFecha;
    bool encontrado=false;
    bool coincidencia=false;
    bool mismoCodigo=false;
    lectura.open("Empleados.txt",ios::in);
    verificador.open("Empleados.txt",ios::in);
    auxiliar.open("auxiliar.txt",ios::out);
    cout<<"***Modificar los datos de un cliente***";
    if(lectura.is_open()&&verificador.is_open()&&auxiliar.is_open())
    {
        fflush(stdin);
        cout<<"Ingresa el c\242digo del cliente que deseas modificar: ";
        getline(cin,auxCodigo);

        if(auxCodigo=="")
        {
            do
            {
                cout<<"c\242digo de Empleado no v\240lido!, intentalo nuevamente: ";
                getline(cin,auxCodigo);
            }
            while(auxCodigo=="");
        }

        codigoModif=auxCodigo;

        getline(lectura,codigoEmpleado);
        while(!lectura.eof())
        {
            getline(lectura,nombre);
            getline(lectura,domicilio);
            getline(lectura,celular);
            getline(lectura,FechaPago);

            if(auxCodigo==codigoEmpleado)
            {
                encontrado=true;
                mostarRegistro(codigoModif);

                cout<<"**********************************************";
                cout<<"\n\n";
                cout<<"Ingresa la nueva informaci\242n para el Empleado\n\n";
                fflush(stdin);
                cout<<"\n\nIngresa el c\242digo del Empleado: ";
                getline(cin,auxCodigo);
                if(auxCodigo==codigoModif)
                {
                    mismoCodigo=true;
                }
                if(mismoCodigo==false)
                {
                    do
                    {
                        if(auxCodigo==codigoModif)
                        {
                            coincidencia=false;
                            break;
                        }
                        verificador.seekg(0);
                        getline(verificador,codigoEmpleado);
                        while(!verificador.eof())
                        {
                            getline(verificador,nombre);
                            getline(verificador,domicilio);
                            getline(verificador,celular);
                            getline(verificador,FechaPago);

                            if(auxCodigo==codigoEmpleado)
                            {
                                coincidencia=true;
                                cout<<"\n\nYa existe un Empleado con ese c\242digo!\n\n";
                                cout<<"El Empleado con ese c\242digo es: "<<nombre<<"\n\n";
                                cout<<"Ingresa un c\242digo v\240lido!: ";
                                getline(cin,auxCodigo);

                                if(auxCodigo=="")
                                {
                                    do
                                    {
                                        cout<<"\nc\242digo de Empleado no v\240lido!, ";
                                        cout<<"intentalo nuevamente: ";
                                        getline(cin,auxCodigo);
                                    }
                                    while(auxCodigo=="");
                                }
                                break;
                            }

                            getline(verificador,codigoEmpleado);
                        }

                        if(verificador.eof()&&auxCodigo!=codigoEmpleado)
                        {
                            coincidencia=false;
                        }

                    }
                    while(coincidencia==true);
                }
                system(CLEAR);
                cout<<"***Modificar los datos de un Eempleado***";
                cout<<"Ingresa el c\242digo del Empleado que deseas modificar: ";
                cout<<codigoModif;
                mostarRegistro(codigoModif);
                cout<<"**********************************************";
                cout<<"\n\n";
                cout<<"\n\nIngresa la nueva informaci\242n para el Empleado";
                cout<<"\n\nIngresa el c\242digo del Empleado: ";
                cout<<auxCodigo;
                cout<<"\n\n";
                fflush(stdin);
                cout<<"\n\nIngresa el nombre del Empleado: ";
                getline(cin,auxNombre);;
                fflush(stdin);
                cout<<"\n\n";
                cout<<"\n\nIngresa el domicilio del Empleado: ";
                getline(cin,auxDomicilio);
                cout<<"\n\n";
                fflush(stdin);
                cout<<"\n\nIngresa el n\243mero de telefono del Empleado: ";
                getline(cin,auxCelular);
                cout<<"\n\n";
                fflush(stdin);
                cout<<"\n\nIngresa la fecha de inscripci\242n del Empleado: ";
                getline(cin,auxFecha);
                cout<<"\n\n";
                auxiliar<<auxCodigo<<"\n"<<auxNombre<<"\n"<<auxDomicilio<<"\n"<<auxCelular
                        <<"\n"<<auxFecha<<"\n";
                cout<<"\n\nEl Registro se ha modificado correctamente.";
            }


            else
            {

                auxiliar<<codigoEmpleado<<"\n"<<nombre<<"\n"<<domicilio<<"\n"<<celular
                        <<"\n"<<FechaPago<<"\n";
            }


            getline(lectura,codigoEmpleado);
        }

    }
    else
    {
        error();
    }

    if(encontrado==false)
    {
        cout<<"\n\nNo se encontr\242 ning\243n registro con ese c\242digo!\n\n";
    }
    lectura.close();
    verificador.close();
    auxiliar.close();
    remove("Empleados.txt");
    rename("auxiliar.txt","Empleados.txt");
    pausa();
}

void menu::listarEmpleado()
{
    int i=0;
    ifstream lectura;
    lectura.open("Empleados.txt",ios::in);
    if(lectura.is_open())
    {
        cout<<"***Listado de todos los Empleados***";
        getline(lectura,codigoEmpleado);
        while(!lectura.eof())
        {
            i++;
            getline(lectura,nombre);
            getline(lectura,domicilio);
            getline(lectura,celular);
            getline(lectura,FechaPago);
            cout<<"C\242digo: "<<codigoEmpleado<<endl;
            cout<<"Nombre: "<<nombre<<endl;
            cout<<"Domicilio: "<<domicilio<<endl;
            cout<<"Celular: "<<celular<<endl;
            cout<<"Fecha de inscripci\242n: "<<FechaPago<<endl;
            cout<<"\n\n";
            getline(lectura,codigoEmpleado);
        }

        if(i==1)
            cout<<"Hay un solo Empleado registrado en esta Empresa ";

        else

            cout<<"Hay un total de "<<i<<" Empleados registrados en esta Empresa";
    }
    else
    {
        error();
    }
    lectura.close();
    pausa();
}

void pausa()
{
    cout<<"Presiona Enter para continuar...";
    getch();
    system(CLEAR);
}

int main()
{
    system ("color f9");
    menu inicio;
    inicio.menuPrincipal();
    return 0;
}

void error()
{
    cout<<"No se pudo abrir el archivo de registros, asegurese que el archivo se encuentre en, ";
    cout<<"la misma ubicaci\242n que el programa o que el archivo de texto se llame: ";
    cout<<"Empleados, si el archivo tiene otro nombre ren\242mbrelo al ya mencionado";
}

void menu::mostarRegistro(string codigo)
{

    ifstream mostrar;
    mostrar.open("Empleados.txt",ios::in);
    getline(mostrar,codigoEmpleado);
    while(!mostrar.eof())
    {
        getline(mostrar,nombre);
        getline(mostrar,domicilio);
        getline(mostrar,celular);
        getline(mostrar,FechaPago);

        if(codigo==codigoEmpleado)
        {
            cout<<"\n\nRegistro Encontrado\n\n";
            cout<<"C\242digo: "<<codigoEmpleado<<endl;
            cout<<"Nombre: "<<nombre<<endl;
            cout<<"Domicilio: "<<domicilio<<endl;
            cout<<"Celular: "<<celular<<endl;
            cout<<"Fecha de inscripci\242n: "<<FechaPago<<endl;
            cout<<"\n\n";
        }

        getline(mostrar,codigoEmpleado);
    }

    mostrar.close();
}
